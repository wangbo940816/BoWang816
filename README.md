
<h1 align="center">Hi 👋, I'm 恪晨</h1> 

<h3 align="center"> Welcome To My Github 👋  </h3>

<h3 align="center">I am a passionate frontend developer from China</h3>

<p align="left"> <img src="https://komarev.com/ghpvc/?username=bowang816&label=Profile%20views&color=0e75b6&style=flat" alt="bowang816" /> </p>

- 📝 I regulary write articles on [恪晨的小站](https://blog.wangboweb.site)、[掘金](https://juejin.cn/user/2049145403882430)、[知乎](https://www.zhihu.com/people/ke-chen-6-83)
- 😉 I record my life on [lifeBlog](https://life.wangboweb.site)
- 📫 How to reach me **bo.wang1016@outlook.com**
- 🏢 I'm currently working at **China Unicom Software Research Institute**
- 🚀 I use daily:
  ![JavaScript](https://img.shields.io/badge/-JavaScript-black?style=plastic&logo=javascript)
  ![Git](https://img.shields.io/badge/-Git-black?style=plastic&logo=git)
  ![GitHub](https://img.shields.io/badge/-GitHub-181717?style=plastic&logo=github)
  ![Next.js](https://img.shields.io/badge/next.js-red?style=plastic&logo=appveyor)
  ![MaterialUI](https://img.shields.io/badge/-MatrialUI-0081CB?style=plastic&logo=material-UI)
- 💻 I work using:
  ![React](https://img.shields.io/badge/-React-3b2e5a?style=plastic&logo=react)
  ![GitLab](https://img.shields.io/badge/-GitLab-FCA121?style=plastic&logo=gitlab)
  ![Jenkins](https://img.shields.io/badge/-Jenkins-black?style=plastic&logo=Jenkins)
  ![HTML5](https://img.shields.io/badge/-HTML5-E34F26?style=plastic&logo=html5&logoColor=white)
  ![CSS3](https://img.shields.io/badge/-CSS3-1572B6?style=plastic&logo=css3)
  
- 🌱 Learning all about:
  ![Node.JS](https://img.shields.io/badge/-Node.JS-black?style=plastic&logo=Node.js) 
  ![Express.JS](https://img.shields.io/badge/-Express.JS-c7b198?style=plastic&logo=Express.JS) 
  ![Next.js](https://img.shields.io/badge/-Next.js-black?style=plastic&logo=Next.js)
  ![MongoDB](https://img.shields.io/badge/-MongoDB-black?style=plastic&logo=mongodb)
 
<div style="display: inline">
  <img src="https://github-readme-stats.vercel.app/api?username=bowang816&show_icons=true" height="120" align="center"/>
  <img src="https://github-readme-stats.vercel.app/api/wakatime?username=BoWang816&layout=compact&custom_title=WakaTime%20Last%207%20Days" height="120" align="center"/>
  <img src="https://github-readme-stats.vercel.app/api/top-langs/?username=bowang816&layout=compact" height="120" align="center"/>
</div>
